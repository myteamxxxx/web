<?php

namespace App\Model\Shopee;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;

use Auth;

class AccShop extends Model
{
	protected $fillable = [
        'uid', 'shop_id', 'shop_name', 'country', 'shop_description', 'created_at', 'updated_at', 
    ];

    public function uid()
    {
        return $this->belongsTo('App\User','uid','id');
    }

    public function checkTT($idshop,$uid)
    {
    	$acc = AccShop::where('shop_id',$idshop)
    		->where('uid',$uid)
    		->count();
    	if ($acc>0) {
    		return true;
    	}else
    		return false;
    }

    public function createAcc($shop = array())
    {
        return AccShop::create($shop);
    }

    public function list()
    {
    	return AccShop::where('uid',Auth::id())->paginate(env('SHOPEE_ROW_LIMIT'));
    }

    public function unlink($id)
    {
        $shop = AccShop::where('shop_id',$id)->where('uid',Auth::id())->firstOrFail();
        return $shop->delete();
    }    
}
