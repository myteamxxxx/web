<?php

namespace App\Exceptions\Shopee;

use Exception;
use Carbon\Carbon;
use GuzzleHttp\Client;
class ShopeeClass extends Exception
{
    public $shopeeAPI;
    public $nodes;

    public function __construct()
    {
        $this->nodes['shop']    = new Nodes\Shop\Shop();
        $this->nodes['order']   = new Nodes\Order\Order();
    }

    public function __get(string $name)
    {
        if (!array_key_exists($name, $this->nodes)) {
            throw new InvalidArgumentException(sprintf('Property "%s" not exists', $name));
        }

        return $this->nodes[$name];
    }
}
