<?php

namespace App\Exceptions\Shopee\Nodes\Order;

use Exception;
use App\Exceptions\Shopee\ShopeeAPI;

class Order extends Exception
{
	public $shopee;
	public $shopid;
	public $create_time_from;
	public $create_time_to;
	public $pagination_entries_per_page = 0;
	public $pagination_offset = 0;
    public $rs_total = array();

    public function __construct()
    {
    	$this->shopee = new ShopeeAPI;
    }

    public function setShopId($shopid): int
    {
    	return $this->shopid = $shopid;
    }

    public function setCreateTime($time)
    {
    	return $this->create_time_from = $time;
    }

    public function setToTime($time)
    {
    	return $this->create_time_to = $time;
    }

    public function setLimit($limit): int
    {
    	return $this->pagination_entries_per_page = $limit;
    }

    public function setPage($page): int
    {
    	return $this->pagination_offset = $page;
    }

    public function getOrdersList($shopid): array
    {
    	$link = 'orders/basics';
    	$this->shopee->shop_id = $shopid;
        $body = array([
            [
                'create_time_from'  =>  $this->create_time_from,
                'create_time_to'    =>  $this->create_time_to,
                'pagination_entries_per_page'   =>  $this->pagination_entries_per_page,
                'pagination_offset' =>  $this->pagination_offset,
            ]
        ]);
    	$rs = $this->shopee->send($link, $body);

        print_r($body);

        if (isset($rs["error"])) {
            return array();
        }else
        {
            if ($rs['more']) {

                echo "goi ve === ".$this->pagination_offset;

                $this->pagination_offset = $this->pagination_offset + 1;

                $rs_next = $this->getOrderListArray($this->getOrdersList($shopid));

                $rss = array_merge($rs_next, $this->getOrdersList($shopid));

                return $rss;
            }
            else
                return $this->getOrderListArray($rs);
        }
    }

    public function queryOrder($shopid,$array): array
    {
        if ($array['error']) {
            return array(); 
        }else{
            $this->pagination_offset++;
            $rs = $this->getOrdersList($shopid);
        }
    }

    public function getOrderListArray($order): array
    {
        /*print_r($order);
        exit;*/
        if ($this->countOrderListArray($order) > 0) {
            return $order;
        }
        else
            return array();        
    }

    public function countOrderListArray($order): int
    {
        return count($order);
    }
}
