<?php

namespace App\Exceptions\Shopee\Nodes\Shop;

use Exception;
use Auth;

use App\Exceptions\Shopee\ShopeeAPI;

class Shop extends Exception
{
	public $shopee;
	public $shopid;

	public function __construct()
	{
		$this->shopee = new ShopeeAPI;
	}

    //lay thong tin cua shop
    public function GetShopInfo($shop_id = 0)
    {
    	$link = 'shop/get';
    	$this->shopee->shop_id = $shop_id;

		return $this->shopee->send($link,array());
    }

    public function GetShopName($shop = array())
    {
    	$shop['uid'] = Auth::id();
    	unset($shop['item_limit']);
    	unset($shop['disable_make_offer']);
    	unset($shop['videos']);
    	unset($shop['request_id']);
    	unset($shop['images']);
    	unset($shop['enable_display_unitno']);
    	return $shop;
    }

    public function genLinkAuth($url_redirect ='')
    {
    	$hash_link = hash('sha256', $this->shopee->key.$url_redirect);

    	return $this->shopee->api_url.'shop/auth_partner?redirect='.$url_redirect
    					.'&token='.$hash_link
    					.'&id='.$this->shopee->partner_id;
	}
}
