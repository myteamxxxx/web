<?php

namespace App\Exceptions\Shopee;

use Exception;
use Carbon\Carbon;
use GuzzleHttp\Client;

class ShopeeAPI extends Exception
{
	public $key;
    public $partner_id;
    public $api_url;    
    public $shop_id;

    public function __construct()
    {
    	if (env('SHOPEE_DEV')==0) {
    		$this->api_url = env('SHOPEE_URL_API_SAN');
	    	$this->key = env('SHOPEE_KEY_SAN');
	    	$this->partner_id = (int)env('SHOPEE_PARTNERID_SAN');
    	}
    	else
    	{
	    	$this->api_url = env('SHOPEE_URL_API');
	    	$this->key = env('SHOPEE_KEY');
	    	$this->partner_id = (int)env('SHOPEE_PARTNERID');
	    }
    }

    public function setShopId($shopid): int
    {
        return $this->shop_id = $shopid;
    }

    // Tạo ra chữ kỹ xác thực 
    public function signature($str): string
    {
    	$str = hash_hmac('sha256', $str, $this->key, false);
    	return $str;
    }

    public function getDefaultParameters(): array
    {
        return [
            'partner_id' => (int)$this->partner_id,
            'shopid' => (int)$this->shop_id,
            'timestamp' => Carbon::now()->timestamp, // Put the current UNIX timestamp when making a request
        ];
    }

    protected function createJsonBody(array $data)
    {
        $data = array_merge($this->getDefaultParameters(), $data);

        return json_encode($data);
    }

    protected function HeaderRequest($auth_key='')
    {
        return ['headers' => [ 
                    'Content-Type' => 'application/json',
                    'Authorization' => $this->signature($auth_key)
                ]];
    }

    public function send($link, $jbody = array())
    {
    	$patch = $this->api_url.$link;

    	$body = $this->createJsonBody($jbody);
    	$auth_key = $patch.'|'.$body;

    	//print_r($body);

    	$client = new Client($this->HeaderRequest($auth_key));

    	$req = $client->request('POST', 
    			$patch ,
    			[
    				'body' => $body
    			]
    		);

    	return json_decode($req->getBody(),true);
    }
}
