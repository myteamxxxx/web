<?php

namespace App\Http\Controllers\Shopee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;

use App\Exceptions\Shopee\ShopeeClass;
use App\Model\Shopee\AccShop;

class ShopeeController extends Controller
{
	public $shopinfo;
	public $shopacc;
	public $shopee;

	public function __construct()
	{
		$this->shopacc = new AccShop;
		$this->shopee = new ShopeeClass;
	}

    public function index()
    {
    	
    }

    public function shop()
    {
        $shop = $this->shopacc->list(Auth::id());

    	return view('shopee.shop.shop',[
            'shop' => $shop]
        );
    }

    public function unlink($shopid)
    {
        $this->shopacc->unlink($shopid);

        flash('Đã hủy liên kết trong hệ thống');

        return redirect()->route('shopee.shop');
    }

    public function verify()
    {
        return redirect()->away($this->shopee->shop->genLinkAuth(route('shopee.verified')));
    }

    public function verified(Request $request)
    {
    	$shop_id = $request->shop_id;

    	if ($this->shopacc->checkTT($shop_id,Auth::id())) {
    		flash('Tài khoản đã tồn tại trong hệ thống')->error();    		
    	}else{

    		$shop = $this->shopee->shop->GetShopInfo($shop_id);

    		//print_r($shop);

    		$this->shopacc->createAcc($this->shopee->shop->GetShopName($shop));

    		flash('Đã liên kết và lưu vào hệ thống');        
    	}

    	return redirect()->route('shopee.shop');
    }

    //api
    public function apiShop()
    {
        return $this->shopacc->getShopInfo();
    }
}
