<?php

namespace App\Http\Controllers\Shopee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Carbon\Carbon;

use App\Exceptions\Shopee\ShopeeClass;

class TestController extends Controller
{
    public $shopinfo;
    public $rs = array();

	public function __construct()
	{
		$this->shopee = new ShopeeClass;
	}

	public function orderlist()
    {       
        $this->shopee->order->setCreateTime(Carbon::now()->subDays(15)->timestamp);
        $this->shopee->order->setToTime(Carbon::now()->timestamp);
        $this->shopee->order->setLimit(10000);
        $this->shopee->order->setPage(0);
        $this->rs = $this->shopee->order->getOrdersList(205134);
        print_r($this->rs);
        //print_r($this->shopee->order->getOrderListArray($rs));
    }

    public function ShopInfo($shopid)
    {
    	$shop = $this->shopee->shop->GetShopInfo(6893399);
    	$this->shopee->shop->GetShopName($shop);
    }
}
