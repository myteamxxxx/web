<?php

namespace App\Http\Controllers\Shopee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\Exceptions\Shopee\ShopeeClass;
use App\Model\Shopee\AccShop;

class OrderController extends Controller
{
	public $shopacc;
	public $shopee;

	public function __construct()
	{
		$this->shopacc = new AccShop;
		$this->shopee = new ShopeeClass;
	}

	public function index()
	{
		$shop = $this->shopacc->list();

		$this->shopee->order->setCreateTime(Carbon::now()->subDays(15)->timestamp);
        $this->shopee->order->setToTime(Carbon::now()->timestamp);
        $this->shopee->order->setLimit(100);
        $this->shopee->order->setPage(0);

        $shop_order = array();

        foreach ($shop as $row) {
        	print_r($row);

        	$rq = $this->shopee->order->getOrdersList($row->shop_id);

        	$array = array([
        		'total'	=>	count($rq),
        	]);
        	$shop_order = array_merge($shop_order,$array);
        }

        return;

		return view('shopee.order.index', [
			'shop'	=>	$shop,
			'shop_order'	=>	$shop_order,
		]);
	}

	public function synOrder()
	{
		$sday = 1;
		$eday = 15;
		$rs = array();

		//dong bo 6 thang gay day nhat
		for ($i=0; $i < 1; $i++) { 
			$sday = ($eday == 15)? 0:$eday; //ngay bat dau
			$if = ($i == 0)? 1: $i;
			$eday = $if * 15; //ngay ket thu

			$this->shopee->order->setCreateTime(Carbon::now()->subDays($eday)->timestamp);
	        $this->shopee->order->setToTime(Carbon::now()->subDays($sday)->timestamp);
	        $this->shopee->order->setLimit(10);
	        $this->shopee->order->setPage(0);

	        //$rs = array_merge($rs,$this->shopee->order->getOrdersList(6893399));
	        
	        print_r($this->shopee->order->getOrdersList(6893399));

	        echo "<br>$i === $sday - $eday <br> <hr>";

		}

		//return $rs;
	}
}
