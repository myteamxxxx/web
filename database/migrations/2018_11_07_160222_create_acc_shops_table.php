<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_shops', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid');
            $table->string('shop_name');
            $table->integer('shop_id');
            $table->string('country')->nullable();
            $table->string('shop_description')->nullable();        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_shops');
    }
}
