<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'thanh-vien'], function() {
    Auth::routes(['verify' => true]);
});

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('quan-ly')->middleware('verified')->group(function(){
	//shopee
    Route::group(['prefix' => 'shopee','namespace'=>'shopee'], function() {
    	//home
        Route::get('/', 'ShopeeController@index')->name('shopee.home');

        Route::get('shop', 'ShopeeController@shop')->name('shopee.shop');
        Route::get('shop/verify', 'ShopeeController@verify')->name('shopee.verify');
        Route::get('shop/verified', 'ShopeeController@verified')->name('shopee.verified');
        Route::get('shop/unlink/{id}', 'ShopeeController@unlink')->name('shopee.unlink')->where('id', '[0-9]+');

        Route::group(['prefix' => 'order'], function() {
            Route::get('/', 'OrderController@index')->name('shopee.order');
            Route::get('syn', 'OrderController@synOrder')->name('shopee.order.syn');
        });

        Route::group(['prefix' => 'test'], function() {
            Route::get('orderlist', 'TestController@orderlist')->name('shopee.orderlist');
            Route::get('shopinfo/{id}', 'TestController@ShopInfo');
        });

        Route::get('api/shop', 'ShopeeController@apiShop')->name('shopee.api.shop');
    });
});
