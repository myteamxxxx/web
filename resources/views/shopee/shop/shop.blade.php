@extends('adminlte::page')

@section('title', 'Quản lý tài khoản Shopee')

@section('content_header')
    <h1>Quản lý tài khoản shopee</h1>
@stop

@section('content')
	<div class="content body">
		<p>
			<a class="btn btn-primary" href="{{ route('shopee.verify') }}" role="button">Thêm tài khoản</a>
		</p>
		<p>@include('flash::message')</p>
		
    	<table id="shop" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Tên Shop</th>
                <th>Ngày Tạo</th>
                <th>Xóa</th>
            </tr>
        </thead> 
        <tbody>
            @foreach ($shop as $row)
                <tr>
                    <td>{{$row->shop_id}}</td>
                    <td>{{$row->shop_name}}</td>
                    <td>{{$row->created_at}}</td>
                    <td><a class="btn btn-default" href="{{ route('shopee.unlink',[$row->shop_id]) }}" role="button">Huy Lien Ket</a></td>
                </tr>
            @endforeach
        </tbody>     
    </table>
	</div>
    <div class="row">
        {!!$shop->links()!!}
    </div>
@stop

@section('adminlte_js')
@stop