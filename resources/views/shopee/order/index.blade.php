@extends('adminlte::page')

@section('title', 'Quản lý đơn hàng')

@section('content')
<div class="content body">
	<p>
		<a class="btn btn-primary" href="{{ route('shopee.shop') }}" role="button">Danh sách tài khoản</a>
	</p>
	<p>@include('flash::message')</p>
    @foreach ($shop as $row)
    	<div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Đơn hàng của tài khoản <b>{{$row->shop_name}}</b></h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Tổng số đơn hàng</span>
                                <span class="info-box-number">{{$shopee->order->countOrderListArray($shopee->order->getOrdersList($row->shop_id))}}<small>%</small></span>
                            </div>
                          <!-- /.info-box-content -->
                        </div>
                    <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

                            <div class="info-box-content">
                              <span class="info-box-text">Đơn hàng mới chưa duyệt</span>
                              <span class="info-box-number">41,410</span>
                            </div>
                          <!-- /.info-box-content -->
                        </div>
                    <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

                    <!-- fix for small devices only -->
                    <div class="clearfix visible-sm-block"></div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

                            <div class="info-box-content">
                              <span class="info-box-text">Đang xử lý</span>
                              <span class="info-box-number">760</span>
                            </div>
                        <!-- /.info-box-content -->
                        </div>
                      <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Đơn Hủy</span>
                                <span class="info-box-number">2,000</span>
                            </div>
                        <!-- /.info-box-content -->
                        </div>
                    <!-- /.info-box -->
                    </div>
            <!-- /.col -->
            </div>
        </div>
        <!-- /.box-body -->
    </div>	
    @endforeach
</div>

@stop

@section('adminlte_js')
@stop